# docker-registry

docker-registry

## settings

- 必須
  - .envファイルに`_ENV_DOCKER_REGISTRY_HOST=${動かすサーバのホスト名}` を追加

- 任意
  - `DOCKER_REGISTRY_PORT`
  - `DOCKER_REGISTRY_DIR`
  - `FRONTEND_PORT`  
  上記を.envに追加

## run

- `docker-compose up -d`を実行
  - 監視用のサーバも動くため、registryサーバのみ起動する場合は`docker-compose up -d docker-registry`
