# MinIO

- https://min.io/

## Initial Setup

```
$ cp .env.sample .env
```

## Run

```
$ docker-compose up -d minio
```

- you can access on 'http://localhost:9000'

## Use MinIO Client

- Run mc command
- command usage: https://docs.min.io/docs/minio-client-complete-guide

```
$ docker-compose run --rm mc ${command}
$ docker-compose run --rm mc ls minio-svc/public
```
