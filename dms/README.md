# Document management system, DMS

- ファイルサーバーが嫌すぎるので、文書管理のサービスを導入する
- 以下のOSSを評価してみる

## Alfresco
- Alfresco Content Repository Community
- https://hub.docker.com/r/alfresco/alfresco-content-repository-community
- Pulls 500K+
- it requires a minimum of 12GB Memory!!

## LogicalDOC
- LogicalDOC DMS - Community Edition
- https://hub.docker.com/r/logicaldoc/logicaldoc-ce
- Pulls 100K+

## OpenKM
- Document Management System and Content Management System
- https://hub.docker.com/r/openkm/openkm-ce
- Pulls 10K+

