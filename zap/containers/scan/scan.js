const axios = require('axios')
const ulid = require('ulid')
const fs = require('fs')
const moment = require('moment')

const TRACE = '\u001b[36m' // trace / log
const DEBUG = '\u001b[32m' // debug
const INFO = '\u001b[34m' // info
const WARN = '\u001b[33m' // warn
const ERROR = '\u001b[31m' // error
const FATAL = '\u001b[35m' // fatal
const RESET = '\u001b[0m'

const contextName = ulid.ulid()
const zap_url = 'http://zap:8090'
const scan_target = 'http://someting/'
const login_url = 'http://someting/auth/local'
const username = 'admin'
const password = 'THcLLBE5WaCrGdsc7jQ5Y3Dw87'
const maxChildren = 10

const post_options = {
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    Accept:
      'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
  },
}

async function sleep(sec) {
  return new Promise(resolve => {
    console.log(TRACE + '  ... sleep ' + sec + ' [sec]')
    setInterval(() => {
      return resolve()
    }, sec * 1000)
  })
}

function debug(res, title) {
  console.error(INFO + 'Request debug start  ---> ' + WARN + title + RESET)
  console.log({
    url: res.config.url,
    method: res.config.method,
    status: res.status,
    data: res.data,
  })
  console.error(TRACE + ' --->  Request debug end' + RESET)
}

async function scan() {
  // セッション初期化
  let res = await axios.get(`${zap_url}/JSON/core/action/newSession/`)
  debug(res, 'セッション初期化')

  // コンテキスト作成
  let params = new URLSearchParams()
  params.append('contextName', contextName)
  res = await axios.post(
    `${zap_url}/JSON/context/action/newContext/`,
    params,
    post_options
  )
  debug(res, 'コンテキスト作成')
  const contextId = res.data.contextId

  // スコープ設定
  params = new URLSearchParams()
  params.append('contextName', contextName)
  params.append('regex', scan_target)
  res = await axios.post(
    `${zap_url}/JSON/context/action/includeInContext/`,
    params,
    post_options
  )
  debug(res, 'スコープ設定')

  let authMethodConfigParams = ''
  authMethodConfigParams += `loginUrl=${login_url}`
  authMethodConfigParams += `&loginRequestData=` + encodeURIComponent(`username=${username}&password=${password}&method=POST`)

  // ログイン方法設定
  params = new URLSearchParams()
  params.append('contextId', contextId)
  params.append('authMethodName', 'formBasedAuthentication')
  params.append('authMethodConfigParams', authMethodConfigParams)
  res = await axios.post(
    `${zap_url}/JSON/authentication/action/setAuthenticationMethod/`,
    params,
    post_options
  )
  debug(res, 'ログイン方法設定')

  // ログインインジケーター設定
  params = new URLSearchParams()
  params.append('contextId', contextId)
  params.append('loggedInIndicatorRegex', 'QE')
  res = await axios.post(
    `${zap_url}/JSON/authentication/action/setLoggedInIndicator/`,
    params,
    post_options
  )
  debug(res, 'ログインインジケーター設定')

  // ユーザ作成
  params = new URLSearchParams()
  params.append('contextId', contextId)
  params.append('name', username)
  res = await axios.post(
    `${zap_url}/JSON/users/action/newUser/`,
    params,
    post_options
  )
  debug(res, 'ユーザ作成')
  const userId = res.data.userId

  // ユーザ有効化
  params = new URLSearchParams()
  params.append('contextId', contextId)
  params.append('userId', userId)
  params.append('enabled', 'true')
  res = await axios.post(
    `${zap_url}/JSON/users/action/setUserEnabled/`,
    params,
    post_options
  )
  debug(res, 'ユーザ有効化')

  // クレデンシャル設定
  let authCredentialsConfigParams = ''
  authCredentialsConfigParams += `username=${username}&password=${password}`
  params = new URLSearchParams()
  params.append('contextId', contextId)
  params.append('userId', userId)
  params.append('authCredentialsConfigParams', authCredentialsConfigParams)
  res = await axios.post(
    `${zap_url}/JSON/users/action/setAuthenticationCredentials/`,
    params,
    post_options
  )
  debug(res, 'クレデンシャル設定')

  // forcedUser
  params = new URLSearchParams()
  params.append('contextId', contextId)
  params.append('userId', userId)
  res = await axios.post(
    `${zap_url}/JSON/forcedUser/action/setForcedUser/`,
    params,
    post_options
  )
  debug(res, 'forcedUser')

  console.log(INFO + '===================================================')
  console.log('    START SCAN    ---> Spider 実行 ')
  console.log('===================================================' + RESET)

  // start spider scan
  params = new URLSearchParams()
  params.append('url', scan_target)
  params.append('contextId', contextId)
  params.append('userId', userId)
  params.append('maxChildren', maxChildren)
  params.append('recurse', 'true')
  res = await axios.post(
    `${zap_url}/JSON/spider/action/scanAsUser/`,
    params,
    post_options
  )
  debug(res, 'start spider scan')
  const spider_id = res.data.scanAsUser

  // Spider 完了まで待機
  let status = '0'
  while (status !== '100') {
    await sleep(1)
    const check = await axios.get(`${zap_url}/JSON/spider/view/status/`, {
      params: { scanId: spider_id },
    })
    // debug(check, 'spider status')
    status = check.data.status
    console.log('spider status -> ' + status)
  }

  console.log(INFO + '===================================================')
  console.log('    START SCAN    ---> ActiveScan 実行 ')
  console.log('===================================================' + RESET)

  // start active scan
  params = new URLSearchParams()
  params.append('url', scan_target)
  params.append('contextId', contextId)
  params.append('userId', userId)
  params.append('maxChildren', maxChildren)
  params.append('recurse', 'true')
  res = await axios.post(
    `${zap_url}/JSON/ascan/action/scanAsUser/`,
    params,
    post_options
  )
  debug(res, 'start active scan')
  const scan_id = res.data.scanAsUser

  // Spider 完了まで待機
  status = '0'
  while (status !== '100') {
    await sleep(5)
    const check = await axios.get(`${zap_url}/JSON/ascan/view/status/`, {
      params: { scanId: scan_id },
    })
    // debug(check, 'active scan status')
    status = check.data.status
    console.log('active scan status -> ' + status)
  }

  console.log(INFO + '----- >>> scan job was finished' + RESET)
}

async function result() {
  console.log(INFO + '===================================================')
  console.log('    Scan  finished. Get result ')
  console.log('===================================================' + RESET)

  res = await axios.get(`${zap_url}/JSON/core/view/urls/`)
  debug(res, 'urls')

  res = await axios.get(`${zap_url}/JSON/params/view/params/`, {
    params: {
      site: scan_target,
    },
  })
  debug(res, 'params')
  const params = res.data.Parameters
  // console.log(JSON.stringify(params, null, '  '))

  res = await axios.get(`${zap_url}/JSON/core/view/alerts/`, {
    params: {
      baseurl: scan_target,
    },
  })
  debug(res, 'json_data result')
  const json = res.data
  // console.log(JSON.stringify(json, null, '  '))

  const timestamp = moment().format('YYYYMMDD-HHmmss')
  fs.writeFileSync(
    '/usr/volumes/reports/' + timestamp + '.json',
    JSON.stringify(json)
  )

  // XML
  res = await axios.get(`${zap_url}/OTHER/core/other/xmlreport/`)
  debug(res, 'xml result')
  fs.writeFileSync('/usr/volumes/reports/' + timestamp + '.xml', res.data)

  // HTML
  res = await axios.get(`${zap_url}/OTHER/core/other/htmlreport/`)
  debug(res, 'xml result')
  fs.writeFileSync('/usr/volumes/reports/' + timestamp + '.html', res.data)

  console.log(INFO + '----- >>> get result job was finished' + RESET)
}


module.exports = function() {
  scan()
  .then(() => {
    return result()
  })
  .then(() => {
    // process.exit(0)
  })
  .catch(e => {
    console.error('-------------------------- Error')
    console.error(e)
    // process.exit(-1)
  })
}
