'use strict'

const express = require('express')
const serveIndex = require('serve-index')

const scan = require('./scan')
let started = false

const app = express()

app.get('/scan', (req, res) => {
  res.status(200).send('<html><body><form method="post" action="scan"><input type="submit" value="start scan" /></form></body></html>')
})
app.post('/scan', (req, res) => {
  if (started) {
    return res.status(200).send('<html><body>scan already started. please wait a moment and access to top <br/> <a href="/">top</a></body></html>')
  } else {
    res.status(200).send('<html><body>scan started. please wait a moment and access to top <br/> <a href="/">top</a></body></html>')
    started = true
    scan().then(() => {
      started = false
    }).catch(e => {
      started = false
    })
  }

})

app.use(express.static('/usr/volumes/reports/'))
app.use(serveIndex('/usr/volumes/reports/', { icons: true }))


app.listen(process.env.PORT || 8091)
