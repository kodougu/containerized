# Use pgAdmin4 with container

## Initial Setup

```
$ cp .env.sample .env
```

- edit `PGADMIN_DEFAULT_EMAIL` and `PGADMIN_DEFAULT_PASSWORD` in `.env` file

```
$ vi .env
```

## Run pgAdmin4

```
$ docker-compose up -d
```

- you can access pgAdmin4 on 'http://localhost:8888'

