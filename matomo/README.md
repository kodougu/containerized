# Matomo

- https://matomo.org/

## Initial Setup

```
$ cp .env.sample .env
```

## Run Metabase

```
$ docker-compose up -d
```

- you can access pgAdmin4 on 'http://localhost:8890'

