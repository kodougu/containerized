#!/bin/sh

docker-compose build
docker-compose run --rm web upgrade
docker-compose up -d smtp memcached redis postgres
sleep 3
docker-compose up -d
