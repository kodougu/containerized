# sentry

Real-time crash reporting for your web apps, mobile apps, and games.


## initial setting
- `.env.example` をコピーし、`.env`を作る
- 秘密鍵を生成する
- 生成した鍵を`.env`へ転記

```
cp -n .env.example .env
docker-compose build
docker-compose run --rm web config generate-secret-key
```

